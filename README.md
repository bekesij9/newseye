# Uses of the Term "Telegraph" in the Context of Journalism

## Introduction to Topic Modeling on a Data Set created by the _Newseye_ Project

A Jupyter Notebook by [János Békési](mailto:janos.bekesi@univie.ac.at) and [Martin Gasteiner](mailto:martin.gasteiner@univie.ac.at), April 2021


## Data

22 MB transkribus OCR result json data (down to article level) resp. 12 MB resulting csv data 
included in the repository in `./data-telegraf`.


## Workflow

Sometimes the generating of topic models is rather tedious, since difficulties can arise regarding
input data (datetime series with different starting or ending points, unforeseen gaps, etc.), or 
the output formatting has to consider presentation quirks or sequence fittings. Though time spent 
will probably be more than estimated, any of those obstacles will be overcome with a bit of
patience and insistence.
 

## The Notebook

The presented Jupyter notebook `workflow.ipynb`contains a complete sequence of processing steps to generate topic models 
from data OCRed by [Transkribus](https://readcoop.eu/transkribus), along with some visualizations to direct 
the interpretation of results. 

To provide some guidance, we tried to prepend most cells with a short explanation; however, an intermediate
skill level regarding [Python](https://python.org) programming and [Jupyter](https://jupyter.org/) notebooks 
might be quite helpful.


